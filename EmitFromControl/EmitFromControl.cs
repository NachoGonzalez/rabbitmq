﻿using System;
using System.Linq;
using RabbitMQ.Client;
using System.Text;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace EmitLogTopic
{
    class EmitFromControl
    {
        public static void Main(string[] args) //se pasara por parametros el molino y señal
        {
            double valor = -1;
            DateTime fechahora = DateTime.Now;
            string senyal = "";
            string json_message = "";
            string conectionstring = "user id=AltrixWriter;password=TirantLoBlanc5;Server=192.168.3.9;Database=";
            bool molino_ok = true;

            if (args.Length < 2)
            {
                senyal = "0";
            }
            else
            {
                senyal = args[1];
            }
            string query ="select TOP 1 FechaHora, valor from Registro where idsenyal = "+senyal+" order by pkid desc";

            if (args[0].Equals("H1")||args[0].Equals("h1")||args[0].Equals("1"))
            {
                conectionstring += "IndcresaH1_Registro";
            }
            else if (args[0].Equals("H2") || args[0].Equals("h2") || args[0].Equals("2"))
            {
                conectionstring += "IndcresaH2_Registro";
            }
            else if (args[0].Equals("H4") || args[0].Equals("h4") || args[0].Equals("4"))
            {
                conectionstring += "IndcresaH4_Registro";
            }
            else if (args[0].Equals("H5") || args[0].Equals("h5") || args[0].Equals("5"))
            {
                conectionstring += "AltrixIndcresaH5_Registro";
            }
            else
            {
                Console.WriteLine(args[0]);
                Console.WriteLine("nok. molino desconocido");
                molino_ok = false;
            }

            if (molino_ok)
            {
                Console.WriteLine(conectionstring);
                Console.WriteLine(query);

                SqlConnection myConnection = new SqlConnection(conectionstring);
                try
                {
                    myConnection.Open();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                SqlCommand cmd = new SqlCommand(query, myConnection);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine("{0}", reader.GetDouble(1));
                    valor = reader.GetDouble(1);
                    Console.WriteLine("{0}", reader.GetDateTime(0));
                    fechahora = reader.GetDateTime(0);
                    Senyal senyal_model = new Senyal(valor,fechahora);
                    json_message = JsonConvert.SerializeObject(senyal_model);
                    Console.WriteLine(json_message);
                }
                myConnection.Close();
                var factory = new ConnectionFactory() { HostName = "localhost" };
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: "topic_logs",
                                            type: "topic");

                    var routingKey = args[0] + "." + senyal;
                    var message = json_message;
                    var body = Encoding.UTF8.GetBytes(message);
                    channel.BasicPublish(exchange: "topic_logs",
                                         routingKey: routingKey,
                                         basicProperties: null,
                                         body: body);
                    Console.WriteLine(" [x] Sent '{0}':'{1}'", routingKey, message);
                }
            }
        }
    }

    public class Senyal
    {
        public double valor;
        public DateTime fechahora;
        public Senyal(double valor, DateTime fechahora)
        {
            this.valor = valor;
            this.fechahora = fechahora;
        }
        
    }
}
