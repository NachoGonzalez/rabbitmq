# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Implementación de RabbitMQ para RealTime Data
* Version 1.0
* Help: https://www.rabbitmq.com/getstarted.html

### How do I get set up? ###

* Instalación del servicio RabbitMQ: https://www.rabbitmq.com/download.html
* Tutorial instalación: https://www.youtube.com/watch?v=gKzKUmtOwR4&list=PLpKjudDy9JyPWp0PQjDbtGxjWVK5kYuAB
* JSON Library Usage: http://www.newtonsoft.com/json

### Contribution guidelines ###

* Control de archivos base:
* Exp1: Send & Recive --> https://www.rabbitmq.com/tutorials/tutorial-one-dotnet.html
* Exp2: NewTasck & Worker --> https://www.rabbitmq.com/tutorials/tutorial-two-dotnet.html
* Exp3: EmitLog & ReciveLogs --> https://www.rabbitmq.com/tutorials/tutorial-three-dotnet.html
* Exp4: EmitLogDirect & ReciveLogsDirect --> https://www.rabbitmq.com/tutorials/tutorial-four-dotnet.html
* Exp5: EmitLogTopic & ReciveLogsTopic --> https://www.rabbitmq.com/tutorials/tutorial-five-dotnet.html
* SQL & JSON: EmitFromControl & ReciveFromUsers

### Who do I talk to? ###

* Owner: Nacho Gonazlez Valero
* Team: IT Antares-SCI